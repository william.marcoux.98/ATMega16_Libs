#define F_CPU 8000000UL
#define  BAUD  9600
#define BAUDRATE  ((8000000L)/(9600L*16L) - 1)
#define BUF_SIZE 255
#define BUF_LIMIT (BUF_SIZE - 1)

#include "avr\io.h"
#include "avr\interrupt.h"
#include <stdlib.h>
#include <util/delay.h>

char wasteChr;
volatile char RxChr = 0;

char firstTest;

char RxBuf[BUF_SIZE];
char *RxHead, *RxTail;

char TxBuf[BUF_SIZE];
char *TxHead, *TxTail;

char tab[20];

void initPortSerie()
{    
    UBRRH = (BAUDRATE>>8);
	UBRRL = BAUDRATE;
    UCSRB = (1<<TXEN)|(1<<RXEN)|(1<<RXCIE);
    UCSRC = (1<<URSEL)|(1<<USBS)|(3<<UCSZ0);

	RxHead = &RxBuf[0];
	RxTail = &RxBuf[0];
	TxHead = &TxBuf[0];
	TxTail = &TxBuf[0];
}

char RxBufEmpty()
{
	return (RxHead == RxTail);
}

char RxBufFull()
{
	return ((RxHead == &RxBuf[BUF_LIMIT] && RxTail == &RxBuf[0]) || (RxHead == RxTail + 1));
}

char TxBufEmpty()
{
	return (TxHead == TxTail);
}

char TxBufFull()
{
	return ((TxHead == &TxBuf[BUF_LIMIT] && TxTail == &TxBuf[0]) || (TxHead == TxTail + 1));
			
}

void AddRxChar(char chrToAdd)
{
	if(!RxBufFull())
	{
		if(RxTail == &RxBuf[BUF_LIMIT])
			RxTail = RxTail - BUF_LIMIT;
		else
			RxTail = RxTail + 1;

		*RxTail = chrToAdd;
	}
}

void RemRxChar()
{
	if(!RxBufEmpty())
	{
		if(RxHead == &RxBuf[BUF_LIMIT])
			RxHead = RxHead - BUF_LIMIT;
		else
			RxHead = RxHead + 1;
		
		*RxHead = 0;

	}
}

void AddTxChar(char chrToAdd)
{
	if(!TxBufFull())
	{
		if(TxTail == &TxBuf[BUF_LIMIT])
			TxTail = TxTail - BUF_LIMIT;
		else
			TxTail = TxTail + 1;

		*TxTail = chrToAdd;
	}
}

void RemTxChar()
{
	if(!TxBufEmpty())
	{
		if(TxHead == &TxBuf[BUF_LIMIT])
			TxHead = TxHead - BUF_LIMIT;
	   	else
			TxHead = TxHead + 1;
		
		*TxHead = 0;
	}
}

char skbhit()
{
	return (!RxBufEmpty());
}

void sputch(char chrToSend)
{

	AddTxChar(chrToSend);
	

	UCSRB |= (1 << 5);
}

void sputs(char *tabPtr)
{
	while(*tabPtr)
	{
		sputch(*tabPtr);
		tabPtr++;
	}
}

char sgetch()
{
	char tmp;
	while(!skbhit());
	tmp = *(RxHead + 1);
	RemRxChar();
	return tmp;
}

char sgetche()
{
	char tmp = sgetch();
	sputch(tmp);
	return tmp;

}

void sgets(char *tabPtr)
{
	char tmp = 0;
	while(tmp != 13)
	{
		tmp = sgetche();
		if(tmp != 13)
			*tabPtr = tmp;

		tabPtr++;
		
	}
}





ISR(USART_RXC_vect)
{
	if(!RxBufFull())
		AddRxChar(UDR);
	else
		wasteChr = UDR;	
}

ISR(USART_UDRE_vect)
{
	
	UDR = *(TxHead + 1);
	RemTxChar(); 
	
	if(TxBufEmpty())
		UCSRB &= ~0x20;
}
