#include "avr/io.h"
#include "avr\interrupt.h"

void I2CInit(unsigned char TWSRsetter, unsigned char TWBRsetter)
{
	TWSR = TWSRsetter; //prescaler to 1
	TWBR = TWBRsetter;//sets SCL to 50kHz
	TWCR = (1<<TWEN); //Enable TWI
}

void I2CStart(void)
{
	TWCR = (1<<TWINT)|(1<<TWSTA)|(1<<TWEN); //send start condition
    while (!(TWCR & (1<<TWINT)));//wait for TWINT flag set
}

void I2CStop(void)
{
	TWCR = (1<<TWINT)|(1<<TWSTO)|(1<<TWEN); //send stop condition
}

void I2CWrite(char data)
{
    TWDR = data;
    TWCR = (1<<TWINT)|(1<<TWEN);
    while (!(TWCR & (1<<TWINT)));
}

char I2CReadACK(void)
{
	TWCR = (1<<TWINT)|(1<<TWEN)|(1<<TWEA);
    while (!(TWCR & (1<<TWINT)));
    return TWDR;
}

char I2CReadNACK(void)
{
	TWCR = (1<<TWINT)|(1<<TWEN);
    while (!(TWCR & (1<<TWINT)));
    return TWDR;
}

void writeTC74(char address, char command, char data)
{
	I2CStart();
	I2CWrite(address);
	I2CWrite(command);
	I2CWrite(data);
	I2CStop();
}

char readTC74(char address)
{
	I2CStart();
	I2CWrite(address);
	I2CWrite(0x00);
	I2CStart();
	I2CWrite(address|0x01);
	char result = I2CReadNACK();
	I2CStop();
	return result;
}