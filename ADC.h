#define F_CPU 8000000UL

#include <avr/io.h>
#include <stdlib.h>
#include <util/delay.h>

void ADCInit(char prescale)
{
	  
    ADMUX = (1<<REFS0); // AREF = AVcc
	ADMUX |= (prescale & 0x07); //Set prescale

	ADCSRA = (1<<ADEN)|(1<<ADPS2)|(1<<ADPS1)|(1<<ADPS0);

}

int readADC(char pin)
{
	ADMUX = (ADMUX & 0xF8)|(pin & 0x07);
	DCSRA |= (1<<ADSC);

	while(ADCSRA & (1<<ADSC));
	
	return (ADCW >> 2);
}
