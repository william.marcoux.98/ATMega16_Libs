#define F_CPU 		8000000UL

#include <stdio.h>
#include <avr/io.h>
#include "util\delay.h"
#include "avr\interrupt.h"
#include "stdbool.h"

#define DDR_SPI DDRB		// PORTB est le port SPI
#define DD_MOSI	5
#define DD_SCK	7
#define DD_SS	4
//Commande pour le 25C160
#define READ 0x03	// commande pour lire
#define WRITE 0x02	// commande pour �crire
#define WREN 0x06	// commande pour enable l'�criture
#define RDSR 0x05	// commande pour lire le status de lecture
#define WRSR 0x01	// commande pour lire le status d'�criture

void SPI_MasterInit(void)
{
	DDR_SPI = (1<<DD_MOSI) | (1<<DD_SCK) | (1<< DD_SS); //set le portB avec les bonnes sorties/entr�es
	SPCR = (1<<SPE) | (1<<MSTR);
}

char SPI_MasterTransmit(char cData)
{
	char readData;
	
	SPDR = cData;
	
	while(!(SPSR & (1<<SPIF)));
	
	readData = SPDR;

	return readData;
}

void selectDevice(void)
{
	PORTB = PINB & 0xEF;
}

void unselectDevice(void)
{
	PORTB = PINB | 0x10;
}

void writeEnable(void)
{
	selectDevice();
	SPI_MasterTransmit(WREN);
	unselectDevice();
}

void writeSingleByte(int adresse, char data)
{
	writeEnable();
	selectDevice();
	SPI_MasterTransmit(WRITE);
	SPI_MasterTransmit(adresse>>8);
	SPI_MasterTransmit(adresse);
	SPI_MasterTransmit(data);
	unselectDevice();
	_delay_ms(5);
}

void writeMultipleByte(int adresseBase, char * data, char longueur)
{

	for(int i = 0; i < longueur;i++)
	{
		writeSingleByte(adresseBase, data[i]);
		adresseBase++;
	}
	
}

char readSingleByte(int adresse)
{
	selectDevice();
	SPI_MasterTransmit(READ);
	SPI_MasterTransmit(adresse>>8);
	SPI_MasterTransmit(adresse);
	char car = SPI_MasterTransmit(0x0);
	unselectDevice();
	return car;
}
